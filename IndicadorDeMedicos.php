<?php

/*
Plugin Name: Indicador de Médicos
Plugin URI: 
Description: Plugin desenvolvido especialmente para o site Registro de Médicos adicionando a funcionalidade de contabilizar indicações de médicos
Version: 1.0
Author: Vitor Rigoni - Lemon Juice Web Apps
Author URI: http://lemonjuicewebapps.com
License: GPL2
*/

define('INDICADOR_ROOT_DIR', plugin_dir_path(__FILE__));
define('INDICADOR_ROOT_URI', plugin_dir_url(__FILE__));
define('INDICADOR_VIEWS', INDICADOR_ROOT_DIR . "views/");
define('INDICADOR_CLASSES', INDICADOR_ROOT_DIR . "classes/");

if (!function_exists('debug')) {
	function debug($value='') {
		require(INDICADOR_VIEWS . "debug.php");
	}
}

require_once(INDICADOR_CLASSES . 'class-indicador-data-access.php');
require_once(INDICADOR_CLASSES . 'class-indicador-mce-buttons.php');
require_once(INDICADOR_CLASSES . 'class-indicador-registration-handler.php');

add_action( 'init', 'indicador_main' );
add_action( 'admin_init', 'indicador_admin_main' );
register_activation_hook( __FILE__, array('IndicadorDataAccess', 'activate') );
add_shortcode( 'indicador', 'indicador_add_form' );

function indicador_main() {
	add_action( 'wp_enqueue_scripts', 'indicador_enqueue_scripts' );
	foreach (array('indicador_registration', 'nopriv_indicador_registration') as $hook) {
		add_action( "wp_ajax_$hook", 'indicador_register' );
	}
}

function indicador_admin_main() {
	$mceButtons = new IndicadorMceButtons();
	$mceButtons->generate_buttons();
}

function indicador_add_form() {
	global $post;
	$nonce = wp_create_nonce( 'indicador-nonce' );
	$db = new IndicadorDataAccess();
	$count = $db->get_post_indications($post->ID);

	
	$view = file_get_contents(INDICADOR_ROOT_URI . "views/form.php?nonce=$nonce&count=$count");
	return $view;
}

function indicador_enqueue_scripts() {
	wp_enqueue_script( 'indicador-script', INDICADOR_ROOT_URI . 'js/indicador.js', array('jquery'), null, true );
	wp_enqueue_style( 'indicador-style', INDICADOR_ROOT_URI . 'css/indicador-styles.css', null, false, false );
	indicador_localize();
}

function indicador_localize() {
	global $post;
	wp_localize_script( 'indicador-script', 'IndicadorAjaxObj', array(
		'url' => admin_url( 'admin-ajax.php' ),
		'post_id' => $post->ID,
		'loading' => INDICADOR_ROOT_URI . 'img/ajax-loader.gif'
		)
	);
}

function indicador_register() {
	$handler = new IndicadorRegistrationHandler();
	$response = array();
	try {

		$handler->save_registration($_POST['post_id'], $_POST['email']);
		$response = array(
			'message' => 'Recomendação efetuada com sucesso!',
			'code' => 200,
			'count' => $handler->get_post_indications($_POST['post_id'])
			);

	} catch (Exception $e) {
		$response = array(
			'message' => $e->getMessage(),
			'code' => $e->getCode()
			);	
	}
	echo json_encode($response);
	wp_die();
	
}
























