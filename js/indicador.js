jQuery.noConflict();
jQuery(document).ready( function($) {

	var feedback = $(".indicador .feedback");
	var indicadorSubmit = $(".indicador .submit");

	$('.indicador button').on('click', function(){
		$(this).siblings('.modal').fadeToggle();
	});

	FormIndicador.onsubmit = function(event) {
		event.preventDefault();

		indicadorSubmit.html("<img src='" + IndicadorAjaxObj.loading + "' />");

		var data = {
			action: "indicador_registration",
			post_id: IndicadorAjaxObj.post_id,
			email: FormIndicador.IndicadorEmail.value
		}

		$.ajax({
			url: IndicadorAjaxObj.url,
			data: data,
			type: 'POST',
			success: function(data, textStatus, jqXHR) {
				var resp = $.parseJSON(data);
				if (resp.code == 200) {
					feedback.addClass("success");
					FormIndicador.IndicadorEmail.value = "";
					$("#IndicadorCount").html(resp.count);
					setTimeout( function() {
						$(".indicador .modal").fadeOut();
					}, 4000);
				} else {
					feedback.addClass("error");
				};

				feedback.html(resp.message).slideDown(200, clearFeedback);

			},
			error: function(jqXHR, textStatus, errorThrown) {

			},
			complete: function() {
				indicadorSubmit.html("Enviar");
			}
		});

		return false;
	};

	function clearFeedback () {
		setTimeout( function() {
			feedback.removeClass('success').removeClass('error').html("").slideUp();
		}, 4000);
	}

	
});


















