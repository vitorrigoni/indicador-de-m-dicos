(function() {
     tinymce.create('tinymce.plugins.Indicador', {
          init : function(ed, url) {
               ed.addButton( 'btn_indicador', {
                    title : 'Formulário do Indicador de Médicos',
                    image : indicadorObj.imgPath + 'tinymce_icon.png',
                    onclick : function() {
                         ed.selection.setContent('[indicador]');
                    }
               });
          },
          createControl : function(n, cm) {
               return null;
          },
     });
     tinymce.PluginManager.add( 'indicador_mce_buttons', tinymce.plugins.Indicador );
})();