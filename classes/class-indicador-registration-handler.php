<?php

class IndicadorRegistrationHandler extends IndicadorDataAccess
{
	
	public function validate_email($email = "")
	{
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) return true;
		else throw new Exception("Endereço de e-mail inválido", 505);
	}

	public function save_registration($post_id, $email)
	{
		$this->validate_email($email);
		$this->check_existing_registration($post_id, $email);
		return $this->insert($post_id, $email);
	}

	public function check_existing_registration($post_id, $email)
	{
		$count = $this->get_post_indications($post_id, $email);
		if ($count > 0) {
			throw new Exception("Já foi feita uma recomendação para este médico utilizando este endereço de e-mail", 506);
		}
	}

}