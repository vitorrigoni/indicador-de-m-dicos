<?php

class IndicadorMceButtons
{
	
	function __construct()
	{	
		
	}	

	public function generate_buttons()
	{
		foreach ( array('post.php','post-new.php') as $hook ) {
			add_action( "admin_head-$hook", array($this, 'localize_buttons') );
		}

		add_filter( 'mce_buttons', array($this, 'indicador_register_buttons') );
		add_filter( 'mce_external_plugins', array($this, 'indicador_add_tinymce_button') );
	}

	function indicador_register_buttons($buttons)
	{
		array_push($buttons, 'indicador_mce_buttons', 'btn_indicador');
		return $buttons;
	}

	function indicador_add_tinymce_button( $plugin_array )
	{
		$plugin_array['indicador_mce_buttons'] = INDICADOR_ROOT_URI . "js/indicador_mce_buttons.js";
		return $plugin_array;
	}

	function localize_buttons()
	{
		$imgPath = INDICADOR_ROOT_URI . 'img/';
		?>
		<script>
		var indicadorObj = {
			imgPath: '<?php echo $imgPath; ?>'
		}
		</script>
		<?php
	}


}