<?php

/**
* 
*/
class IndicadorDataAccess
{
	
	public function activate()
	{
		global $wpdb;
		$create = "CREATE TABLE IF NOT EXISTS `".$wpdb->prefix."indicador` (`id` int(11) unsigned NOT NULL AUTO_INCREMENT, `post_id` bigint(20) unsigned NOT NULL, `email` varchar(255) DEFAULT NULL, `created` datetime NOT NULL, PRIMARY KEY (`id`), KEY `FK_Indicador_Posts` (`post_id`), CONSTRAINT `FK_Indicador_Posts` FOREIGN KEY (`post_id`) REFERENCES `".$wpdb->prefix."posts` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
		return $wpdb->query($create);
	}

	public function insert($post_id, $email)
	{
		global $wpdb;
		return $wpdb->insert(
				$wpdb->prefix . "indicador",
				array(
					'post_id' => $post_id,
					'email' => $email
				),
				array('%d', '%s')
			);
	}

	public function get_post_indications($post_id, $email = "")
	{
		global $wpdb;
		$q = "";
		if (empty($email)) {
			$q = $wpdb->prepare("SELECT COUNT(id) FROM " . $wpdb->prefix ."indicador WHERE post_id = %d", $post_id);
		} else {
			$q = $wpdb->prepare("SELECT COUNT(id) FROM " . $wpdb->prefix ."indicador WHERE post_id = %d AND email = '%s'", $post_id, $email);	
		}
		return $wpdb->get_var($q);
	}

}

















