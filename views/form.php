<?php
$nonce = $_GET['nonce'];
$count = $_GET['count'];
?>
<div class="indicador">
	<div class="modal">
		<form action="" name="FormIndicador" method="post">
			<div class="feedback"></div>
			<input type="hidden" value="<?php echo $nonce; ?>" />
			<input class="common-input" name="IndicadorEmail" type="text" value="" placeholder="Insira seu e-mail:" />
			<button class="submit invicta_button" type="submit">Enviar</button>
		</form>
	</div>
	<span class="count" id="IndicadorCount" title="Total de recomendações deste médico"><?php echo $count; ?></span>
	<button class="invicta_button">Recomende este médico!</button>
</div>